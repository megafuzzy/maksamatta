# Project description

A confidential project for a small startup related to debt collection business. 

# My role in development and technologies used

- This was my first React / Javascript development project ever
- I worked on this from July 2019 to April 2020
- I was responsible for the frontend development while another developer implemented the backend, api and devOps
- Material-UI components
- Paytrail integration
- Integration to Telia identification broker service (OpenID Connect)

